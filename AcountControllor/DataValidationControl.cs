﻿using CommonLibrary;
using CommonLibrary.Context;
using CommonLibrary.Customer;
using CommonLibrary.Manager;
using System;
using System.Text.RegularExpressions;
using System.Linq;
using System.Collections.Generic;

namespace AcountControllor
{
    public static class DataValidationControl
    {
      

        public static void CustomerDataValidation(this Customer customer)
        {
            if (!CheckingNames(customer.FirstName) || !CheckingNames(customer.MiddleName) || !CheckingNames(customer.LastName))
            {
                throw new ArgumentException("Customer name  credentials are incorect");
            }
            if (!ChakingPhoneNumber(customer.PhoneNumber))
            {
                throw new ArgumentException("Incorect phone number");
            }
            if (string.IsNullOrEmpty(customer.PassportCode))
            {
                throw new ArgumentException("Passport code cant be null or empty");
            }
            if (!CustomerPassportCodeChaking(customer.PassportCode))
            {
                throw new ArgumentException("With this passport code customer  already exists");
            }
            if (string.IsNullOrEmpty(customer.DrivingLicence))
            {
                throw new ArgumentException("Driving licence cant be null or empty");
            }
            if (!CustomerDrivingLicenceChaking(customer.DrivingLicence))
            {
                throw new ArgumentException("With this driving licence customer  already exists");
            }

        }

        public static void ManagerDataValidation(this Manager manager)
        {
            DateTime validationDataTime1 = new DateTime(2002, 1, 1);
            DateTime validationDataTime2 = new DateTime(1949, 01, 01);
            if (!CheckingNames(manager.FirstName) || !CheckingNames(manager.MiddleName) || !CheckingNames(manager.LastName))
            {
                throw new ArgumentException("User name  credentials are incorect");
            }
            if (!(manager.BDay < validationDataTime1 && manager.BDay > validationDataTime2))
            {
                throw new ArgumentException("User birth day is incorect");
            }
            if (!ChakingPhoneNumber(manager.Phonenumber))
            {
                throw new ArgumentException("Incorect phone number");
            }
            if (string.IsNullOrEmpty(manager.UserName) )
            {
                throw new ArgumentException("please enter the username");
            }
            if (!UserNameValidation(manager.UserName))
            {
                throw new ArgumentException("Username already exists");
            }
            if (manager.Password.Length < 6)
            {
                throw new ArgumentException("Invalid password , please enter corect password");
            }

        }
        private static bool CheckingNames(string chakingName)
        {

            Regex rg = new Regex(@"^[a-zA-Z\s]+$");
            bool validation = rg.IsMatch(chakingName);
            return validation;

        }
        private static bool ChakingPhoneNumber(string chakingNumber)
        {

            Regex regex = new Regex(@"[+374][0-9]{2}[0-9]{6}");
            var validation = regex.IsMatch(chakingNumber);
            return validation;


        }
        private static bool UserNameValidation(string userName)
        {
            using(ApplicationContext db = new ApplicationContext())
            {
                List<Manager> managers =( from u in db.Managers
                                where u.UserName == userName
                                select u).ToList();
               
                if (managers.Count() == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                

            }
        }
        private static bool CustomerPassportCodeChaking(string passportCode)
        {
            using(ApplicationContext db = new ApplicationContext())
            {
                List<Customer> customers =( from c in db.Customers
                                    where c.PassportCode.Equals(passportCode)
                                    select c).ToList();

                if (customers.Count() == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        private static bool CustomerDrivingLicenceChaking(string drivingLicence)
        {
            using (ApplicationContext db = new ApplicationContext())
            {
                List<Customer> customers = (from c in db.Customers
                                            where c.DrivingLicence.Equals(drivingLicence)
                                            select c).ToList();

                if (customers.Count() == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
    }
}

