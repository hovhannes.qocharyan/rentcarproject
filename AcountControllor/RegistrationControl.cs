﻿using CommonLibrary.Context;
using CommonLibrary.Customer;
using CommonLibrary.Manager;
using System;
using System.Collections.Generic;
using System.Text;

namespace AcountControllor
{
   public static  class RegistrationControl
    {
        public static Manager ManagerRegistration(string fName, string mName, string lName, DateTime bDay,string phoneNumber, string userName, string password)
        {

            Manager manager = new Manager(fName, mName, lName, bDay,phoneNumber, userName, password);
            manager.ManagerDataValidation();
            using (var context = new ApplicationContext())
            {
                context.Managers.Add(manager);
                context.SaveChanges();
            }
            return manager;


        }

        public static Customer CustomerRegistration(string fName,string mName , string lName , string phoneNumber , DateTime bDay,string passportCode , string drivingLicence)
        {
            Customer customer = new Customer(fName, mName, lName, phoneNumber, bDay, passportCode, drivingLicence);
            customer.CustomerDataValidation();
            using(var context = new ApplicationContext())
            {
                context.Customers.Add(customer);
                context.SaveChanges();
            }
            return customer;
        }
    }
}
