﻿using AcountControllor;
using CommonControl;
using CommonLibrary.Manager;
using ManagerWorkingPage;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginPage
{
    public partial class Login : Form
    {
        public Login()
        {
           
            InitializeComponent();
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            Registration registrationPage = new Registration();
            registrationPage.ShowDialog();
            this.Dispose();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            string userName = textBoxUserName.Text;
            string passWord = textBoxPassword.Text;
            string encPass= Helper.Md5Encryption(passWord);
            LoginControl loginControl = new LoginControl();
            bool isValidUser =loginControl.CheckingUserByUsernameAndPassword(userName,encPass , out Manager manager);
            if (isValidUser)
            {
                ManagerWorkingPage.CarOrder.mg = manager;
                ManagerWorkingPage.ManagerWorkingPage managerWorking = new ManagerWorkingPage.ManagerWorkingPage();
                managerWorking.ShowDialog();
            }
            else
            {
                MessageBox.Show("Incorect username or password");
            }
        }
    }
}
