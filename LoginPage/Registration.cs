﻿using AcountControllor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginPage
{
    public partial class Registration : Form
    {
        public Registration()
        {
            InitializeComponent();
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            try
            {


                string fName = textBoxFName.Text;
                string mName = textBoxMName.Text;
                string lName = textBoxLName.Text;
                DateTime bDay = new DateTime(int.Parse(textBoxYear.Text), int.Parse(textBoxMonth.Text), int.Parse(textBoxDay.Text));
                string phoneNumber = textBoxPhoneNumber.Text;
                string userName = textBoxUserName.Text;
                string password1 = textBoxPassword1.Text;
                string password2 = textBoxPassword2.Text;
                if (!password1.Equals(password2)||string.IsNullOrEmpty(password1))
                {
                    throw new ArgumentException("Password is incorect ,please try again");
                }
                RegistrationControl.ManagerRegistration(fName, mName, lName, bDay, phoneNumber, userName, password1);

                MessageBox.Show("Registration complite");
                textBoxFName.Text = null;
                textBoxMName.Text = null;
                textBoxLName.Text = null;
                textBoxYear.Text = null;
                textBoxMonth.Text = null;
                textBoxDay.Text = null;
                textBoxPhoneNumber.Text = null;
                textBoxUserName.Text = null;
                textBoxPassword1.Text = null;
                textBoxPassword2.Text = null;

                
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            
            }
           
            

        }
    }
}
