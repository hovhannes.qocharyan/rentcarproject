﻿namespace LoginPage
{
    partial class Registration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelFName = new System.Windows.Forms.Label();
            this.labelMName = new System.Windows.Forms.Label();
            this.labelLName = new System.Windows.Forms.Label();
            this.labelBDay = new System.Windows.Forms.Label();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();
            this.labelPassword1 = new System.Windows.Forms.Label();
            this.labelPasword2 = new System.Windows.Forms.Label();
            this.buttonRegistration = new System.Windows.Forms.Button();
            this.textBoxFName = new System.Windows.Forms.TextBox();
            this.textBoxMName = new System.Windows.Forms.TextBox();
            this.textBoxLName = new System.Windows.Forms.TextBox();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.textBoxPassword1 = new System.Windows.Forms.TextBox();
            this.textBoxPassword2 = new System.Windows.Forms.TextBox();
            this.textBoxDay = new System.Windows.Forms.TextBox();
            this.textBoxMonth = new System.Windows.Forms.TextBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.labelday = new System.Windows.Forms.Label();
            this.labelmonth = new System.Windows.Forms.Label();
            this.labelyear = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelFName
            // 
            this.labelFName.AutoSize = true;
            this.labelFName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFName.Location = new System.Drawing.Point(12, 9);
            this.labelFName.Name = "labelFName";
            this.labelFName.Size = new System.Drawing.Size(83, 16);
            this.labelFName.TabIndex = 0;
            this.labelFName.Text = "First Name";
            // 
            // labelMName
            // 
            this.labelMName.AutoSize = true;
            this.labelMName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMName.Location = new System.Drawing.Point(12, 48);
            this.labelMName.Name = "labelMName";
            this.labelMName.Size = new System.Drawing.Size(100, 16);
            this.labelMName.TabIndex = 1;
            this.labelMName.Text = "Middle Name";
            // 
            // labelLName
            // 
            this.labelLName.AutoSize = true;
            this.labelLName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLName.Location = new System.Drawing.Point(12, 87);
            this.labelLName.Name = "labelLName";
            this.labelLName.Size = new System.Drawing.Size(82, 16);
            this.labelLName.TabIndex = 2;
            this.labelLName.Text = "Last Name";
            // 
            // labelBDay
            // 
            this.labelBDay.AutoSize = true;
            this.labelBDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBDay.Location = new System.Drawing.Point(12, 126);
            this.labelBDay.Name = "labelBDay";
            this.labelBDay.Size = new System.Drawing.Size(71, 16);
            this.labelBDay.TabIndex = 3;
            this.labelBDay.Text = "Birth Day";
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhoneNumber.Location = new System.Drawing.Point(12, 165);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(110, 16);
            this.labelPhoneNumber.TabIndex = 4;
            this.labelPhoneNumber.Text = "Phone Number";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUserName.Location = new System.Drawing.Point(12, 204);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(79, 16);
            this.labelUserName.TabIndex = 5;
            this.labelUserName.Text = "Username";
            // 
            // labelPassword1
            // 
            this.labelPassword1.AutoSize = true;
            this.labelPassword1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassword1.Location = new System.Drawing.Point(12, 243);
            this.labelPassword1.Name = "labelPassword1";
            this.labelPassword1.Size = new System.Drawing.Size(76, 16);
            this.labelPassword1.TabIndex = 6;
            this.labelPassword1.Text = "Password";
            // 
            // labelPasword2
            // 
            this.labelPasword2.AutoSize = true;
            this.labelPasword2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPasword2.Location = new System.Drawing.Point(12, 282);
            this.labelPasword2.Name = "labelPasword2";
            this.labelPasword2.Size = new System.Drawing.Size(76, 16);
            this.labelPasword2.TabIndex = 7;
            this.labelPasword2.Text = "Password";
            // 
            // buttonRegistration
            // 
            this.buttonRegistration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegistration.Location = new System.Drawing.Point(227, 363);
            this.buttonRegistration.Name = "buttonRegistration";
            this.buttonRegistration.Size = new System.Drawing.Size(134, 53);
            this.buttonRegistration.TabIndex = 16;
            this.buttonRegistration.Text = "Registration";
            this.buttonRegistration.UseVisualStyleBackColor = true;
            this.buttonRegistration.Click += new System.EventHandler(this.buttonRegistration_Click);
            // 
            // textBoxFName
            // 
            this.textBoxFName.Location = new System.Drawing.Point(189, 12);
            this.textBoxFName.Name = "textBoxFName";
            this.textBoxFName.Size = new System.Drawing.Size(349, 20);
            this.textBoxFName.TabIndex = 8;
            // 
            // textBoxMName
            // 
            this.textBoxMName.Location = new System.Drawing.Point(190, 44);
            this.textBoxMName.Name = "textBoxMName";
            this.textBoxMName.Size = new System.Drawing.Size(349, 20);
            this.textBoxMName.TabIndex = 9;
            // 
            // textBoxLName
            // 
            this.textBoxLName.Location = new System.Drawing.Point(190, 83);
            this.textBoxLName.Name = "textBoxLName";
            this.textBoxLName.Size = new System.Drawing.Size(349, 20);
            this.textBoxLName.TabIndex = 10;
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(190, 161);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(349, 20);
            this.textBoxPhoneNumber.TabIndex = 12;
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(190, 200);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(349, 20);
            this.textBoxUserName.TabIndex = 13;
            // 
            // textBoxPassword1
            // 
            this.textBoxPassword1.Location = new System.Drawing.Point(190, 239);
            this.textBoxPassword1.Name = "textBoxPassword1";
            this.textBoxPassword1.Size = new System.Drawing.Size(349, 20);
            this.textBoxPassword1.TabIndex = 14;
            this.textBoxPassword1.UseSystemPasswordChar = true;
            // 
            // textBoxPassword2
            // 
            this.textBoxPassword2.Location = new System.Drawing.Point(190, 278);
            this.textBoxPassword2.Name = "textBoxPassword2";
            this.textBoxPassword2.Size = new System.Drawing.Size(349, 20);
            this.textBoxPassword2.TabIndex = 15;
            this.textBoxPassword2.UseSystemPasswordChar = true;
            // 
            // textBoxDay
            // 
            this.textBoxDay.Location = new System.Drawing.Point(227, 122);
            this.textBoxDay.Name = "textBoxDay";
            this.textBoxDay.Size = new System.Drawing.Size(70, 20);
            this.textBoxDay.TabIndex = 17;
            // 
            // textBoxMonth
            // 
            this.textBoxMonth.Location = new System.Drawing.Point(347, 122);
            this.textBoxMonth.Name = "textBoxMonth";
            this.textBoxMonth.Size = new System.Drawing.Size(70, 20);
            this.textBoxMonth.TabIndex = 18;
            // 
            // textBoxYear
            // 
            this.textBoxYear.Location = new System.Drawing.Point(468, 122);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(70, 20);
            this.textBoxYear.TabIndex = 19;
            // 
            // labelday
            // 
            this.labelday.AutoSize = true;
            this.labelday.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelday.Location = new System.Drawing.Point(192, 122);
            this.labelday.Name = "labelday";
            this.labelday.Size = new System.Drawing.Size(29, 13);
            this.labelday.TabIndex = 20;
            this.labelday.Text = "Day";
            // 
            // labelmonth
            // 
            this.labelmonth.AutoSize = true;
            this.labelmonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelmonth.Location = new System.Drawing.Point(303, 122);
            this.labelmonth.Name = "labelmonth";
            this.labelmonth.Size = new System.Drawing.Size(42, 13);
            this.labelmonth.TabIndex = 21;
            this.labelmonth.Text = "Month";
            // 
            // labelyear
            // 
            this.labelyear.AutoSize = true;
            this.labelyear.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelyear.Location = new System.Drawing.Point(423, 122);
            this.labelyear.Name = "labelyear";
            this.labelyear.Size = new System.Drawing.Size(33, 13);
            this.labelyear.TabIndex = 22;
            this.labelyear.Text = "Year";
            // 
            // Registration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.ClientSize = new System.Drawing.Size(566, 450);
            this.Controls.Add(this.labelyear);
            this.Controls.Add(this.labelmonth);
            this.Controls.Add(this.labelday);
            this.Controls.Add(this.textBoxYear);
            this.Controls.Add(this.textBoxMonth);
            this.Controls.Add(this.textBoxDay);
            this.Controls.Add(this.buttonRegistration);
            this.Controls.Add(this.textBoxPassword2);
            this.Controls.Add(this.textBoxPassword1);
            this.Controls.Add(this.textBoxUserName);
            this.Controls.Add(this.textBoxPhoneNumber);
            this.Controls.Add(this.textBoxLName);
            this.Controls.Add(this.textBoxMName);
            this.Controls.Add(this.textBoxFName);
            this.Controls.Add(this.labelPasword2);
            this.Controls.Add(this.labelPassword1);
            this.Controls.Add(this.labelUserName);
            this.Controls.Add(this.labelPhoneNumber);
            this.Controls.Add(this.labelBDay);
            this.Controls.Add(this.labelLName);
            this.Controls.Add(this.labelMName);
            this.Controls.Add(this.labelFName);
            this.Name = "Registration";
            this.Text = "Registration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelFName;
        private System.Windows.Forms.Label labelMName;
        private System.Windows.Forms.Label labelLName;
        private System.Windows.Forms.Label labelBDay;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label labelPassword1;
        private System.Windows.Forms.Label labelPasword2;
        private System.Windows.Forms.Button buttonRegistration;
        private System.Windows.Forms.TextBox textBoxFName;
        private System.Windows.Forms.TextBox textBoxMName;
        private System.Windows.Forms.TextBox textBoxLName;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.TextBox textBoxPassword1;
        private System.Windows.Forms.TextBox textBoxPassword2;
        private System.Windows.Forms.TextBox textBoxDay;
        private System.Windows.Forms.TextBox textBoxMonth;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.Label labelday;
        private System.Windows.Forms.Label labelmonth;
        private System.Windows.Forms.Label labelyear;
    }
}