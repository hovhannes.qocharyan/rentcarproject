﻿using CommonLibrary;
using CommonLibrary.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CarControllor
{
    public static class CarDataValidationControl
    {
        public static void CarDataValidation(this Car car)
        {
            if (string.IsNullOrEmpty(car.VinCode) || car.VinCode.Length != 17)
            {
                throw new ArgumentException("Car vin code is incorect");
            }
            if (!CheckingVinCode(car.VinCode))
            {
                throw new ArgumentException("VinCode already exists");
            }

            if (string.IsNullOrEmpty(car.CarModel))
            {
                throw new ArgumentException("Car model can not be null");
            }

            if (string.IsNullOrEmpty(car.CarClass))
            {
                throw new ArgumentException("Car class can not be null");
            }
            if (string.IsNullOrEmpty(car.Color))
            {
                throw new ArgumentException("Please enter car color");
            }

        }

        private static bool CheckingVinCode(string vinCode)
        {
           using (var db = new ApplicationContext())
            {
                var cars = from c in db.Cars
                           where c.VinCode.Equals(vinCode)
                           select c;


                if (cars.Count() == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

             
            }
          

        }
         
    }
}
