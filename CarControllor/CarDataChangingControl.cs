﻿using CommonLibrary;
using CommonLibrary.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace CarControllor
{
    public class CarDataChangingControl
    {
        public void UpdateCarData(Car car)
        {
            using(var db = new ApplicationContext())
            {
                var temp = db.Cars.Find(car.VinCode);
                if (temp == null)
                {
                    return;
                }

                db.Entry(temp).CurrentValues.SetValues(car);
                db.SaveChanges();

            }
        }
    }
}
