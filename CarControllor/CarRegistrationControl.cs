﻿using CommonLibrary;
using CommonLibrary.Context;
using System;

namespace CarControllor
{
    public static class CarRegistrationControl
    {
        public static void CarRegistration(string vincode, string carModel , string carClass ,string color ,decimal price )
        {
            Car car = new Car(vincode, carModel, carClass, color, price);
            car.CarDataValidation();
            using(var db = new ApplicationContext())
            {
                db.Cars.Add(car);
                db.SaveChanges();
            }
        }
    }
}
