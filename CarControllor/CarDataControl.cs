﻿using CommonLibrary;
using CommonLibrary.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace CarControllor
{
    public class CarDataControl
    {
        public List<Car> GetFreeCars()
        {
            List<Car> carList = new List<Car>();
            using (var db = new ApplicationContext())
            {
                var cars = from c in db.Cars
                           where c.InRent == false
                           select c;
                carList = cars.ToList();
            }

            return carList;
        }


        public List<Car> GetCarsInOrder()
        {
            List<Car> carList = new List<Car>();
            using(var db= new ApplicationContext())
            {
                var cars = from c in db.Cars
                           where c.InRent == true
                           select c;

                carList = cars.ToList();

            }
            return carList;
        }
        public List<Car> GetCarsByModel(string carModel)
        {
            List<Car> carList = new List<Car>();
            using (var db = new ApplicationContext())
            {
                var cars = from c in db.Cars
                           where EF.Functions.Like(c.CarModel, $"%{carModel}%")
                           select c;

                carList = cars.ToList();
            }
            return carList;
        }

        public List<Car> GetCarsByColor(string color)
        {
            List<Car> carList = new List<Car>();
            using(var db = new ApplicationContext())
            {
                var cars = from c in db.Cars
                           where EF.Functions.Like(c.Color, $"%{color}%")
                           select c;

                carList = cars.ToList();
            }

            return carList;
        }

        public List<Car> GetCarByVinCode(string vinCode)
        {
            using(var db = new ApplicationContext())
            {
                var car = from c in db.Cars
                          where c.VinCode.Equals(vinCode)
                          select c;

                List<Car> cars = car.ToList();
                return cars ;
            }
        }
    }
}
