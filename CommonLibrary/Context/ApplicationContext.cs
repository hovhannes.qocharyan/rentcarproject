﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using CommonLibrary;
using Microsoft.EntityFrameworkCore;



namespace CommonLibrary.Context
{
   public  class ApplicationContext :DbContext
    {

        public DbSet<Car> Cars { get; set; }
        public DbSet<Customer.Customer> Customers { get; set; }
        public DbSet<Manager.Manager> Managers { get; set; }
        public DbSet<Order.Order> Orders { get; set; }
        public ApplicationContext()
        {

           // Database.ExecuteSqlCommand("ALTER TABLE [Cars] REBUILD WITH (IGNORE_DUP_KEY = ON)");
            Database.EnsureCreated();
        }

        private string jsonFilePath = @"C:\Users\hovhannes.qocharyan\source\repos\CarRentProject\rentcarproject\CommonLibrary\Context\appsettings.json";
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile(jsonFilePath);
            var config = builder.Build();
            string connectionString = config.GetConnectionString("CarRentDB");
            optionsBuilder.UseSqlServer(connectionString);
        }

    }
}
