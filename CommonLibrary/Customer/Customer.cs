﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonLibrary.Customer
{
   public class Customer
    {
        public int CustomerId { get; private set; }
        [MaxLength(25), Required]
        public string FirstName { get; set; }
        [MaxLength(25), Required]

        public string MiddleName { get; set; }
        [MaxLength(25), Required]

        public string LastName { get; set; }

        [Required]
        public string PhoneNumber { get; set; }

        public DateTime BDay { get; set; }
        [Required]

        public string PassportCode { get; private set; }
        [Required]

        public string DrivingLicence { get; private set; }
        public Customer()
        {

        }
        public Customer(string fName, string mName, string lName,string phoneNumber, DateTime bDay, string pasdCode, string drivLic)
        {
            FirstName = fName;
            MiddleName = mName;
            LastName = lName;
            PhoneNumber = phoneNumber;
            BDay = bDay;
            PassportCode = pasdCode;
            DrivingLicence = drivLic;
        }
    }
}
