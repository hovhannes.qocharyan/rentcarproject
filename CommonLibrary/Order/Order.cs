﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CommonLibrary.Order
{
    public class Order
    {
        public int OrderId { get; set; }
        [Required]
        virtual public Customer.Customer Customer { get; set; }
        [Required]
        virtual public Manager.Manager Manager { get; set; }
        [Required]
        virtual public Car Car { get; set; }
        [Required]
        public decimal TotalPrice { get; set; }
        [Required]
        public DateTime StartTime { get; set; }
        [Required]
        public DateTime EndTime { get; set; }

        public Order()
        {

        }
        public Order(Customer.Customer cust, Manager.Manager mang, Car car, decimal totalP, DateTime startTime, DateTime endTime)
        {
            Customer = cust;
            Manager = mang;
            Car = car;
            TotalPrice = totalP;
            StartTime = startTime;
            EndTime = endTime;
        }
    }
}
