﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CommonLibrary
{
    public class Car
    {

        
        [KeyAttribute, Required]
        public string VinCode { get; private set; }
        [MaxLength(25), Required]
        public string CarModel { get; set; }
        [MaxLength(15), Required]
        public string CarClass { get; set; }
        [MaxLength(20), Required]
        public string Color { get; set; }
        [Required]
        public decimal Price { get; private set; }
        [Required]
        public bool InRent { get; set; }
        
       
        public Car()
        {

        }
        public Car(string vinCode, string model, string carClass, string color, decimal price)
        {
            VinCode = vinCode;
            CarModel = model;
            CarClass = carClass;
            Color = color;
            Price = price;
           
            

        }
    }
}
