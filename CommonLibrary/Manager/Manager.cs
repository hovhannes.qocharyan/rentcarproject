﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace CommonLibrary.Manager
{
    public class Manager
    {
        private string password;
        public int ManagerId { get; private set; }
        [MaxLength(25), Required]
        public string FirstName { get; set; }
        [MaxLength(25), Required]

        public string MiddleName { get; set; }
        [MaxLength(25), Required]

        public string LastName { get; set; }

        [Required]
        public DateTime BDay { get; set; }
        [Required]
        public string Phonenumber { get; set; }
        [MaxLength(20), Required]
        public string UserName { get; set; }
        [Required]
        public string Password
        {
            get
            {
                return password;
            }
            private set
            {
                using (MD5 md5 = MD5.Create())
                {
                    UTF8Encoding utf8 = new UTF8Encoding();
                    byte[] data = md5.ComputeHash(utf8.GetBytes(value));
                    password = Convert.ToBase64String(data);
                }
            }
        }

        public Manager()
        {

        }
        public Manager(string FName, string MName, string LName, DateTime bDay,string phoneNumber, string username, string password)
        {

            FirstName = FName;
            MiddleName = MName;
            LastName = LName;
            BDay = bDay;
            Phonenumber = phoneNumber;
            UserName = username;
            Password = password;


        }

    }
}

