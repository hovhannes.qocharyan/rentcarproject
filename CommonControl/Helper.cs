﻿using CommonLibrary.Context;
using CommonLibrary.Customer;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using CommonLibrary;
using CommonLibrary.Manager;

namespace CommonControl
{
    public static class Helper
    {
        public static string Md5Encryption(string password)
        {

            using (MD5 md5 = MD5.Create())
            {
                UTF8Encoding utf8 = new UTF8Encoding();
                byte[] data = md5.ComputeHash(utf8.GetBytes(password));
                password = Convert.ToBase64String(data);
                return password;
            }
        }
        public static Customer GetCustomer(string passportCode)
        {
            using(var db = new ApplicationContext())
            {
                var query = from q in db.Customers
                            where q.PassportCode.Equals(passportCode)
                            select q;

                var temp = (query.ToList()).First();
                Customer customer = new Customer(temp.FirstName, temp.MiddleName, temp.LastName, temp.PhoneNumber, temp.BDay, temp.PassportCode, temp.DrivingLicence);
                return customer;
            }
        }
        public static Car GetCar(string vinCode)
        {
            using(var db = new ApplicationContext())
            {
                var query = from q in db.Cars
                            where q.VinCode.Equals(vinCode)
                            select q;
                var temp = (query.ToList()).First();
                Car car = new Car(temp.VinCode, temp.CarModel, temp.CarClass, temp.Color, temp.Price);
                return car;
            }
        }

        public static Manager GetManager(string userName)
        {
            using (var db = new ApplicationContext())
            {
                var query = from q in db.Managers
                            where q.UserName.Equals(userName)
                            select q;

                var temp = (query.ToList()).First();
                Manager manager = new Manager(temp.FirstName, temp.MiddleName, temp.LastName, temp.BDay, temp.Phonenumber, temp.UserName, temp.Password);
                return manager;
            }
                
        }
        
    }
}
