﻿using CommonLibrary;
using CommonLibrary.Context;
using CommonLibrary.Customer;
using CommonLibrary.Manager;
using CommonLibrary.Order;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForTesting
{
    
    class Program
    {
        static void Main(string[] args)
        {
            
            Customer customer = new Customer("Mhere", "Albert", "Bagdasaryan", "+37477471767", new DateTime(1990, 06, 03), "AT123456", "Dl123456");
            Car car = new Car("WDD12345678912345", "Mercedes", "S", "Silver", 35000);
            Manager manager = new Manager("Admin", "Admin", "Admin", new DateTime(1980, 07, 07), "+37477889944", "admin.admin", "Aa123456");
            Order order = new Order(customer, manager, car, 70000, new DateTime(2020, 01, 01), new DateTime(2020, 01, 01));
            using (var db = new ApplicationContext())
            {
                db.Orders.Add(order); 
                db.SaveChanges();
              
                
               

                


                Console.ReadKey();
            }
        }
    }
}
