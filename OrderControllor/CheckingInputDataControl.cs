﻿using CommonLibrary.Context;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace OrderControllor
{
    public static  class CheckingInputDataControl
    {
        public static bool PassportCodeChecking(string passportCode)
        {
            using(var db = new ApplicationContext())
            {
                var customers = (from c in db.Customers
                                where c.PassportCode.Equals(passportCode)
                                select c).ToList();
                if (customers.Count() == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }


            }
        }
        public static bool VinCodeChacking(string vinCode)
        {
            using(var db = new ApplicationContext())
            {
                var temp = (from c in db.Cars
                           where c.VinCode.Equals(vinCode)
                           select c).ToList();
                var car = temp.FirstOrDefault();
                if (car == null)
                {
                    return false;
                }
                else
                {
                    if (car.InRent == true)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

        public static bool RentDateChaking(DateTime startDate , DateTime endDate)
        {
            if (startDate < endDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
