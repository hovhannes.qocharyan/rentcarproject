﻿using CommonLibrary;
using CommonLibrary.Context;
using CommonLibrary.Customer;
using CommonLibrary.Manager;
using CommonLibrary.Order;
using System;
using System.Linq;

namespace OrderControllor
{
    public class OrderControl
    {
        public void DoOrder(Customer customer , Manager manager , Car car ,decimal totalP ,DateTime startTime, DateTime endTime)
        {
            
            


                if (totalP == 0)
                {
                    string days = (endTime - startTime).ToString();
                    string[] temp = days.Split('.');
                    int rentDays = int.Parse(temp.First());
                    totalP = car.Price * rentDays;
                }
                Order order = new Order(customer, manager, car, totalP, startTime, endTime);
                using (var db = new ApplicationContext())
                {
                    db.Orders.Add(order);
                    db.SaveChanges();
                }
            
           
        }

        
    }
}
