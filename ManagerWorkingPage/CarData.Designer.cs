﻿namespace ManagerWorkingPage
{
    partial class CarData
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.labelGetinCarData = new System.Windows.Forms.Label();
            this.labelSerchingBy = new System.Windows.Forms.Label();
            this.radioButtonFreeCars = new System.Windows.Forms.RadioButton();
            this.radioButtonInOrderCars = new System.Windows.Forms.RadioButton();
            this.radioButtonByCarModel = new System.Windows.Forms.RadioButton();
            this.radioButtonByColor = new System.Windows.Forms.RadioButton();
            this.radioButtonVinCode = new System.Windows.Forms.RadioButton();
            this.textBoxSearchingAttribute = new System.Windows.Forms.TextBox();
            this.labelSerching = new System.Windows.Forms.Label();
            this.dataGridViewCars = new System.Windows.Forms.DataGridView();
            this.buttonSerch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCars)).BeginInit();
            this.SuspendLayout();
            // 
            // labelGetinCarData
            // 
            this.labelGetinCarData.AutoSize = true;
            this.labelGetinCarData.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGetinCarData.Location = new System.Drawing.Point(215, 32);
            this.labelGetinCarData.Name = "labelGetinCarData";
            this.labelGetinCarData.Size = new System.Drawing.Size(225, 31);
            this.labelGetinCarData.TabIndex = 0;
            this.labelGetinCarData.Text = "Geting Car Data";
            // 
            // labelSerchingBy
            // 
            this.labelSerchingBy.AutoSize = true;
            this.labelSerchingBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSerchingBy.Location = new System.Drawing.Point(24, 117);
            this.labelSerchingBy.Name = "labelSerchingBy";
            this.labelSerchingBy.Size = new System.Drawing.Size(26, 16);
            this.labelSerchingBy.TabIndex = 1;
            this.labelSerchingBy.Text = "By";
            // 
            // radioButtonFreeCars
            // 
            this.radioButtonFreeCars.AutoSize = true;
            this.radioButtonFreeCars.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonFreeCars.Location = new System.Drawing.Point(56, 117);
            this.radioButtonFreeCars.Name = "radioButtonFreeCars";
            this.radioButtonFreeCars.Size = new System.Drawing.Size(94, 20);
            this.radioButtonFreeCars.TabIndex = 2;
            this.radioButtonFreeCars.TabStop = true;
            this.radioButtonFreeCars.Text = "Free Cars";
            this.radioButtonFreeCars.UseVisualStyleBackColor = true;
            this.radioButtonFreeCars.CheckedChanged += new System.EventHandler(this.radioButtonFreeCars_CheckedChanged);
            // 
            // radioButtonInOrderCars
            // 
            this.radioButtonInOrderCars.AutoSize = true;
            this.radioButtonInOrderCars.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonInOrderCars.Location = new System.Drawing.Point(56, 140);
            this.radioButtonInOrderCars.Name = "radioButtonInOrderCars";
            this.radioButtonInOrderCars.Size = new System.Drawing.Size(117, 20);
            this.radioButtonInOrderCars.TabIndex = 3;
            this.radioButtonInOrderCars.TabStop = true;
            this.radioButtonInOrderCars.Text = "In Order Cars";
            this.radioButtonInOrderCars.UseVisualStyleBackColor = true;
            this.radioButtonInOrderCars.CheckedChanged += new System.EventHandler(this.radioButtonInOrderCars_CheckedChanged);
            // 
            // radioButtonByCarModel
            // 
            this.radioButtonByCarModel.AutoSize = true;
            this.radioButtonByCarModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonByCarModel.Location = new System.Drawing.Point(56, 164);
            this.radioButtonByCarModel.Name = "radioButtonByCarModel";
            this.radioButtonByCarModel.Size = new System.Drawing.Size(97, 20);
            this.radioButtonByCarModel.TabIndex = 4;
            this.radioButtonByCarModel.TabStop = true;
            this.radioButtonByCarModel.Text = "Car Model";
            this.radioButtonByCarModel.UseVisualStyleBackColor = true;
            this.radioButtonByCarModel.CheckedChanged += new System.EventHandler(this.radioButtonByCarModel_CheckedChanged);
            // 
            // radioButtonByColor
            // 
            this.radioButtonByColor.AutoSize = true;
            this.radioButtonByColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonByColor.Location = new System.Drawing.Point(56, 187);
            this.radioButtonByColor.Name = "radioButtonByColor";
            this.radioButtonByColor.Size = new System.Drawing.Size(63, 20);
            this.radioButtonByColor.TabIndex = 6;
            this.radioButtonByColor.TabStop = true;
            this.radioButtonByColor.Text = "Color";
            this.radioButtonByColor.UseVisualStyleBackColor = true;
            this.radioButtonByColor.CheckedChanged += new System.EventHandler(this.radioButtonByColor_CheckedChanged);
            // 
            // radioButtonVinCode
            // 
            this.radioButtonVinCode.AutoSize = true;
            this.radioButtonVinCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonVinCode.Location = new System.Drawing.Point(56, 210);
            this.radioButtonVinCode.Name = "radioButtonVinCode";
            this.radioButtonVinCode.Size = new System.Drawing.Size(98, 20);
            this.radioButtonVinCode.TabIndex = 8;
            this.radioButtonVinCode.TabStop = true;
            this.radioButtonVinCode.Text = "Vind Code";
            this.radioButtonVinCode.UseVisualStyleBackColor = true;
            this.radioButtonVinCode.CheckedChanged += new System.EventHandler(this.radioButtonVinCode_CheckedChanged);
            // 
            // textBoxSearchingAttribute
            // 
            this.textBoxSearchingAttribute.Location = new System.Drawing.Point(212, 183);
            this.textBoxSearchingAttribute.Name = "textBoxSearchingAttribute";
            this.textBoxSearchingAttribute.Size = new System.Drawing.Size(304, 20);
            this.textBoxSearchingAttribute.TabIndex = 9;
            // 
            // labelSerching
            // 
            this.labelSerching.AutoSize = true;
            this.labelSerching.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSerching.Location = new System.Drawing.Point(218, 164);
            this.labelSerching.Name = "labelSerching";
            this.labelSerching.Size = new System.Drawing.Size(138, 16);
            this.labelSerching.TabIndex = 10;
            this.labelSerching.Text = "Searching attribute";
            // 
            // dataGridViewCars
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCars.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewCars.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewCars.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewCars.Location = new System.Drawing.Point(3, 309);
            this.dataGridViewCars.Name = "dataGridViewCars";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCars.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewCars.Size = new System.Drawing.Size(661, 341);
            this.dataGridViewCars.TabIndex = 11;
            // 
            // buttonSerch
            // 
            this.buttonSerch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSerch.Location = new System.Drawing.Point(212, 242);
            this.buttonSerch.Name = "buttonSerch";
            this.buttonSerch.Size = new System.Drawing.Size(140, 33);
            this.buttonSerch.TabIndex = 12;
            this.buttonSerch.Text = "Search";
            this.buttonSerch.UseVisualStyleBackColor = true;
            this.buttonSerch.Click += new System.EventHandler(this.buttonSerch_Click);
            // 
            // CarData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.buttonSerch);
            this.Controls.Add(this.dataGridViewCars);
            this.Controls.Add(this.labelSerching);
            this.Controls.Add(this.textBoxSearchingAttribute);
            this.Controls.Add(this.radioButtonVinCode);
            this.Controls.Add(this.radioButtonByColor);
            this.Controls.Add(this.radioButtonByCarModel);
            this.Controls.Add(this.radioButtonInOrderCars);
            this.Controls.Add(this.radioButtonFreeCars);
            this.Controls.Add(this.labelSerchingBy);
            this.Controls.Add(this.labelGetinCarData);
            this.Name = "CarData";
            this.Size = new System.Drawing.Size(667, 653);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCars)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelGetinCarData;
        private System.Windows.Forms.Label labelSerchingBy;
        private System.Windows.Forms.RadioButton radioButtonFreeCars;
        private System.Windows.Forms.RadioButton radioButtonInOrderCars;
        private System.Windows.Forms.RadioButton radioButtonByCarModel;
        private System.Windows.Forms.RadioButton radioButtonByColor;
        private System.Windows.Forms.RadioButton radioButtonVinCode;
        private System.Windows.Forms.TextBox textBoxSearchingAttribute;
        private System.Windows.Forms.Label labelSerching;
        private System.Windows.Forms.DataGridView dataGridViewCars;
        private System.Windows.Forms.Button buttonSerch;
    }
}
