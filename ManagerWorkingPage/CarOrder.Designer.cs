﻿namespace ManagerWorkingPage
{
    partial class CarOrder
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCarOrder = new System.Windows.Forms.Label();
            this.labelCustomerPassportCode = new System.Windows.Forms.Label();
            this.labelCarVinCode = new System.Windows.Forms.Label();
            this.labelTotalPrice = new System.Windows.Forms.Label();
            this.labelStartTime = new System.Windows.Forms.Label();
            this.labelEndTime = new System.Windows.Forms.Label();
            this.textBoxCustomerPassportCode = new System.Windows.Forms.TextBox();
            this.textBoxCarVinCode = new System.Windows.Forms.TextBox();
            this.textBoxStartDay = new System.Windows.Forms.TextBox();
            this.textBoxTotalPrice = new System.Windows.Forms.TextBox();
            this.labelCustomerPassportCodeCheckSimvole = new System.Windows.Forms.Label();
            this.labelCarVinCodeCheckingSimvole = new System.Windows.Forms.Label();
            this.labelTotalPriceChakingSimvole = new System.Windows.Forms.Label();
            this.textBoxStartMonth = new System.Windows.Forms.TextBox();
            this.labelDay = new System.Windows.Forms.Label();
            this.labelmonth = new System.Windows.Forms.Label();
            this.labelyear = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxEndMonth = new System.Windows.Forms.TextBox();
            this.textBoxEndDay = new System.Windows.Forms.TextBox();
            this.buttonChakeOrderDate = new System.Windows.Forms.Button();
            this.buttonOrder = new System.Windows.Forms.Button();
            this.textBoxStartYear = new System.Windows.Forms.TextBox();
            this.textBoxEndYear = new System.Windows.Forms.TextBox();
            this.labelStartDateValidationSimvole = new System.Windows.Forms.Label();
            this.labelEndDateChaikingSimvole = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelCarOrder
            // 
            this.labelCarOrder.AutoSize = true;
            this.labelCarOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarOrder.Location = new System.Drawing.Point(238, 24);
            this.labelCarOrder.Name = "labelCarOrder";
            this.labelCarOrder.Size = new System.Drawing.Size(143, 31);
            this.labelCarOrder.TabIndex = 0;
            this.labelCarOrder.Text = "Car Order";
            // 
            // labelCustomerPassportCode
            // 
            this.labelCustomerPassportCode.AutoSize = true;
            this.labelCustomerPassportCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerPassportCode.Location = new System.Drawing.Point(22, 119);
            this.labelCustomerPassportCode.Name = "labelCustomerPassportCode";
            this.labelCustomerPassportCode.Size = new System.Drawing.Size(209, 20);
            this.labelCustomerPassportCode.TabIndex = 1;
            this.labelCustomerPassportCode.Text = "Customer Passport Code";
            // 
            // labelCarVinCode
            // 
            this.labelCarVinCode.AutoSize = true;
            this.labelCarVinCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarVinCode.Location = new System.Drawing.Point(22, 155);
            this.labelCarVinCode.Name = "labelCarVinCode";
            this.labelCarVinCode.Size = new System.Drawing.Size(110, 20);
            this.labelCarVinCode.TabIndex = 2;
            this.labelCarVinCode.Text = "Car VinCode";
            // 
            // labelTotalPrice
            // 
            this.labelTotalPrice.AutoSize = true;
            this.labelTotalPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalPrice.Location = new System.Drawing.Point(22, 263);
            this.labelTotalPrice.Name = "labelTotalPrice";
            this.labelTotalPrice.Size = new System.Drawing.Size(89, 20);
            this.labelTotalPrice.TabIndex = 3;
            this.labelTotalPrice.Text = "TotalPrice";
            // 
            // labelStartTime
            // 
            this.labelStartTime.AutoSize = true;
            this.labelStartTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStartTime.Location = new System.Drawing.Point(22, 191);
            this.labelStartTime.Name = "labelStartTime";
            this.labelStartTime.Size = new System.Drawing.Size(93, 20);
            this.labelStartTime.TabIndex = 4;
            this.labelStartTime.Text = "Start Date";
            // 
            // labelEndTime
            // 
            this.labelEndTime.AutoSize = true;
            this.labelEndTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEndTime.Location = new System.Drawing.Point(22, 227);
            this.labelEndTime.Name = "labelEndTime";
            this.labelEndTime.Size = new System.Drawing.Size(85, 20);
            this.labelEndTime.TabIndex = 5;
            this.labelEndTime.Text = "End Date";
            // 
            // textBoxCustomerPassportCode
            // 
            this.textBoxCustomerPassportCode.Location = new System.Drawing.Point(281, 118);
            this.textBoxCustomerPassportCode.Name = "textBoxCustomerPassportCode";
            this.textBoxCustomerPassportCode.Size = new System.Drawing.Size(309, 20);
            this.textBoxCustomerPassportCode.TabIndex = 6;
            // 
            // textBoxCarVinCode
            // 
            this.textBoxCarVinCode.Location = new System.Drawing.Point(281, 157);
            this.textBoxCarVinCode.Name = "textBoxCarVinCode";
            this.textBoxCarVinCode.Size = new System.Drawing.Size(309, 20);
            this.textBoxCarVinCode.TabIndex = 7;
            // 
            // textBoxStartDay
            // 
            this.textBoxStartDay.Location = new System.Drawing.Point(319, 193);
            this.textBoxStartDay.Name = "textBoxStartDay";
            this.textBoxStartDay.Size = new System.Drawing.Size(60, 20);
            this.textBoxStartDay.TabIndex = 8;
            // 
            // textBoxTotalPrice
            // 
            this.textBoxTotalPrice.Location = new System.Drawing.Point(281, 265);
            this.textBoxTotalPrice.Name = "textBoxTotalPrice";
            this.textBoxTotalPrice.Size = new System.Drawing.Size(309, 20);
            this.textBoxTotalPrice.TabIndex = 10;
            this.textBoxTotalPrice.Text = "0";
            // 
            // labelCustomerPassportCodeCheckSimvole
            // 
            this.labelCustomerPassportCodeCheckSimvole.AutoSize = true;
            this.labelCustomerPassportCodeCheckSimvole.Location = new System.Drawing.Point(597, 125);
            this.labelCustomerPassportCodeCheckSimvole.Name = "labelCustomerPassportCodeCheckSimvole";
            this.labelCustomerPassportCodeCheckSimvole.Size = new System.Drawing.Size(0, 13);
            this.labelCustomerPassportCodeCheckSimvole.TabIndex = 11;
            // 
            // labelCarVinCodeCheckingSimvole
            // 
            this.labelCarVinCodeCheckingSimvole.AutoSize = true;
            this.labelCarVinCodeCheckingSimvole.Location = new System.Drawing.Point(596, 160);
            this.labelCarVinCodeCheckingSimvole.Name = "labelCarVinCodeCheckingSimvole";
            this.labelCarVinCodeCheckingSimvole.Size = new System.Drawing.Size(0, 13);
            this.labelCarVinCodeCheckingSimvole.TabIndex = 12;
            // 
            // labelTotalPriceChakingSimvole
            // 
            this.labelTotalPriceChakingSimvole.AutoSize = true;
            this.labelTotalPriceChakingSimvole.Location = new System.Drawing.Point(597, 271);
            this.labelTotalPriceChakingSimvole.Name = "labelTotalPriceChakingSimvole";
            this.labelTotalPriceChakingSimvole.Size = new System.Drawing.Size(0, 13);
            this.labelTotalPriceChakingSimvole.TabIndex = 15;
            // 
            // textBoxStartMonth
            // 
            this.textBoxStartMonth.Location = new System.Drawing.Point(429, 193);
            this.textBoxStartMonth.Name = "textBoxStartMonth";
            this.textBoxStartMonth.Size = new System.Drawing.Size(60, 20);
            this.textBoxStartMonth.TabIndex = 16;
            // 
            // labelDay
            // 
            this.labelDay.AutoSize = true;
            this.labelDay.Location = new System.Drawing.Point(287, 200);
            this.labelDay.Name = "labelDay";
            this.labelDay.Size = new System.Drawing.Size(26, 13);
            this.labelDay.TabIndex = 18;
            this.labelDay.Text = "Day";
            // 
            // labelmonth
            // 
            this.labelmonth.AutoSize = true;
            this.labelmonth.Location = new System.Drawing.Point(386, 200);
            this.labelmonth.Name = "labelmonth";
            this.labelmonth.Size = new System.Drawing.Size(37, 13);
            this.labelmonth.TabIndex = 19;
            this.labelmonth.Text = "Month";
            // 
            // labelyear
            // 
            this.labelyear.AutoSize = true;
            this.labelyear.Location = new System.Drawing.Point(495, 196);
            this.labelyear.Name = "labelyear";
            this.labelyear.Size = new System.Drawing.Size(29, 13);
            this.labelyear.TabIndex = 20;
            this.labelyear.Text = "Year";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(495, 232);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "Year";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(386, 236);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Month";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "Day";
            // 
            // textBoxEndMonth
            // 
            this.textBoxEndMonth.Location = new System.Drawing.Point(429, 229);
            this.textBoxEndMonth.Name = "textBoxEndMonth";
            this.textBoxEndMonth.Size = new System.Drawing.Size(60, 20);
            this.textBoxEndMonth.TabIndex = 22;
            // 
            // textBoxEndDay
            // 
            this.textBoxEndDay.Location = new System.Drawing.Point(319, 229);
            this.textBoxEndDay.Name = "textBoxEndDay";
            this.textBoxEndDay.Size = new System.Drawing.Size(60, 20);
            this.textBoxEndDay.TabIndex = 21;
            // 
            // buttonChakeOrderDate
            // 
            this.buttonChakeOrderDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChakeOrderDate.Location = new System.Drawing.Point(440, 307);
            this.buttonChakeOrderDate.Name = "buttonChakeOrderDate";
            this.buttonChakeOrderDate.Size = new System.Drawing.Size(149, 32);
            this.buttonChakeOrderDate.TabIndex = 27;
            this.buttonChakeOrderDate.Text = "Check";
            this.buttonChakeOrderDate.UseVisualStyleBackColor = true;
            this.buttonChakeOrderDate.Click += new System.EventHandler(this.buttonChakeOrderDate_Click);
            // 
            // buttonOrder
            // 
            this.buttonOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOrder.Location = new System.Drawing.Point(440, 356);
            this.buttonOrder.Name = "buttonOrder";
            this.buttonOrder.Size = new System.Drawing.Size(149, 32);
            this.buttonOrder.TabIndex = 28;
            this.buttonOrder.Text = "Order";
            this.buttonOrder.UseVisualStyleBackColor = true;
            this.buttonOrder.Click += new System.EventHandler(this.buttonOrder_Click);
            // 
            // textBoxStartYear
            // 
            this.textBoxStartYear.Location = new System.Drawing.Point(529, 191);
            this.textBoxStartYear.Name = "textBoxStartYear";
            this.textBoxStartYear.Size = new System.Drawing.Size(60, 20);
            this.textBoxStartYear.TabIndex = 17;
            // 
            // textBoxEndYear
            // 
            this.textBoxEndYear.Location = new System.Drawing.Point(529, 227);
            this.textBoxEndYear.Name = "textBoxEndYear";
            this.textBoxEndYear.Size = new System.Drawing.Size(60, 20);
            this.textBoxEndYear.TabIndex = 23;
            // 
            // labelStartDateValidationSimvole
            // 
            this.labelStartDateValidationSimvole.AutoSize = true;
            this.labelStartDateValidationSimvole.Location = new System.Drawing.Point(596, 197);
            this.labelStartDateValidationSimvole.Name = "labelStartDateValidationSimvole";
            this.labelStartDateValidationSimvole.Size = new System.Drawing.Size(0, 13);
            this.labelStartDateValidationSimvole.TabIndex = 29;
            // 
            // labelEndDateChaikingSimvole
            // 
            this.labelEndDateChaikingSimvole.AutoSize = true;
            this.labelEndDateChaikingSimvole.Location = new System.Drawing.Point(595, 232);
            this.labelEndDateChaikingSimvole.Name = "labelEndDateChaikingSimvole";
            this.labelEndDateChaikingSimvole.Size = new System.Drawing.Size(0, 13);
            this.labelEndDateChaikingSimvole.TabIndex = 30;
            // 
            // CarOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.labelEndDateChaikingSimvole);
            this.Controls.Add(this.labelStartDateValidationSimvole);
            this.Controls.Add(this.buttonOrder);
            this.Controls.Add(this.buttonChakeOrderDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxEndYear);
            this.Controls.Add(this.textBoxEndMonth);
            this.Controls.Add(this.textBoxEndDay);
            this.Controls.Add(this.labelyear);
            this.Controls.Add(this.labelmonth);
            this.Controls.Add(this.labelDay);
            this.Controls.Add(this.textBoxStartYear);
            this.Controls.Add(this.textBoxStartMonth);
            this.Controls.Add(this.labelTotalPriceChakingSimvole);
            this.Controls.Add(this.labelCarVinCodeCheckingSimvole);
            this.Controls.Add(this.labelCustomerPassportCodeCheckSimvole);
            this.Controls.Add(this.textBoxTotalPrice);
            this.Controls.Add(this.textBoxStartDay);
            this.Controls.Add(this.textBoxCarVinCode);
            this.Controls.Add(this.textBoxCustomerPassportCode);
            this.Controls.Add(this.labelEndTime);
            this.Controls.Add(this.labelStartTime);
            this.Controls.Add(this.labelTotalPrice);
            this.Controls.Add(this.labelCarVinCode);
            this.Controls.Add(this.labelCustomerPassportCode);
            this.Controls.Add(this.labelCarOrder);
            this.Name = "CarOrder";
            this.Size = new System.Drawing.Size(667, 653);
            this.Load += new System.EventHandler(this.CarOrder_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCarOrder;
        private System.Windows.Forms.Label labelCustomerPassportCode;
        private System.Windows.Forms.Label labelCarVinCode;
        private System.Windows.Forms.Label labelTotalPrice;
        private System.Windows.Forms.Label labelStartTime;
        private System.Windows.Forms.Label labelEndTime;
        private System.Windows.Forms.TextBox textBoxCustomerPassportCode;
        private System.Windows.Forms.TextBox textBoxCarVinCode;
        private System.Windows.Forms.TextBox textBoxStartDay;
        private System.Windows.Forms.TextBox textBoxTotalPrice;
        private System.Windows.Forms.Label labelCustomerPassportCodeCheckSimvole;
        private System.Windows.Forms.Label labelCarVinCodeCheckingSimvole;
        private System.Windows.Forms.Label labelTotalPriceChakingSimvole;
        private System.Windows.Forms.TextBox textBoxStartMonth;
        private System.Windows.Forms.Label labelDay;
        private System.Windows.Forms.Label labelmonth;
        private System.Windows.Forms.Label labelyear;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxEndMonth;
        private System.Windows.Forms.TextBox textBoxEndDay;
        private System.Windows.Forms.Button buttonChakeOrderDate;
        private System.Windows.Forms.Button buttonOrder;
        private System.Windows.Forms.TextBox textBoxStartYear;
        private System.Windows.Forms.TextBox textBoxEndYear;
        private System.Windows.Forms.Label labelStartDateValidationSimvole;
        private System.Windows.Forms.Label labelEndDateChaikingSimvole;
    }
}
