﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManagerWorkingPage
{
    public partial class ManagerWorkingPage : Form
    {
        public ManagerWorkingPage()
        {
            InitializeComponent();
        }

        private void ManagerWorkingPage_Load(object sender, EventArgs e)
        {

        }

        private void buttonCarRegistration_Click(object sender, EventArgs e)
        {
            carRegistration2.BringToFront();
        }

        private void carRegistration1_Load(object sender, EventArgs e)
        {

        }

        private void buttonCarData_Click(object sender, EventArgs e)
        {
            carData2.BringToFront();
        }

        private void buttonCarDataChanging_Click(object sender, EventArgs e)
        {
            carDataChanging1.BringToFront();
        }

        private void buttonCustomerRegistration_Click(object sender, EventArgs e)
        {
            customerRegistration1.BringToFront();
        }

        private void buttonCarOrder_Click(object sender, EventArgs e)
        {
            carOrder1.BringToFront();
        }
    }
}
