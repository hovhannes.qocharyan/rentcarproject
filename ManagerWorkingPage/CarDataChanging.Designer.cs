﻿namespace ManagerWorkingPage
{
    partial class CarDataChanging
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCarDataChanging = new System.Windows.Forms.Label();
            this.labelVinCode = new System.Windows.Forms.Label();
            this.textBoxVinCodeForSearching = new System.Windows.Forms.TextBox();
            this.labelCarModel = new System.Windows.Forms.Label();
            this.labelCarClass = new System.Windows.Forms.Label();
            this.labelColor = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxCarModel = new System.Windows.Forms.TextBox();
            this.textBoxCarClass = new System.Windows.Forms.TextBox();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.textBoxPriceForDay = new System.Windows.Forms.TextBox();
            this.checkBoxinRent = new System.Windows.Forms.CheckBox();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonChange = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelCarDataChanging
            // 
            this.labelCarDataChanging.AutoSize = true;
            this.labelCarDataChanging.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarDataChanging.Location = new System.Drawing.Point(199, 33);
            this.labelCarDataChanging.Name = "labelCarDataChanging";
            this.labelCarDataChanging.Size = new System.Drawing.Size(263, 31);
            this.labelCarDataChanging.TabIndex = 0;
            this.labelCarDataChanging.Text = "Car Data Changing";
            // 
            // labelVinCode
            // 
            this.labelVinCode.AutoSize = true;
            this.labelVinCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVinCode.Location = new System.Drawing.Point(26, 136);
            this.labelVinCode.Name = "labelVinCode";
            this.labelVinCode.Size = new System.Drawing.Size(184, 20);
            this.labelVinCode.TabIndex = 1;
            this.labelVinCode.Text = "Vincode for searching";
            // 
            // textBoxVinCodeForSearching
            // 
            this.textBoxVinCodeForSearching.Location = new System.Drawing.Point(250, 136);
            this.textBoxVinCodeForSearching.Name = "textBoxVinCodeForSearching";
            this.textBoxVinCodeForSearching.Size = new System.Drawing.Size(302, 20);
            this.textBoxVinCodeForSearching.TabIndex = 2;
            // 
            // labelCarModel
            // 
            this.labelCarModel.AutoSize = true;
            this.labelCarModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarModel.Location = new System.Drawing.Point(27, 268);
            this.labelCarModel.Name = "labelCarModel";
            this.labelCarModel.Size = new System.Drawing.Size(86, 18);
            this.labelCarModel.TabIndex = 3;
            this.labelCarModel.Text = "Car Model";
            // 
            // labelCarClass
            // 
            this.labelCarClass.AutoSize = true;
            this.labelCarClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarClass.Location = new System.Drawing.Point(27, 310);
            this.labelCarClass.Name = "labelCarClass";
            this.labelCarClass.Size = new System.Drawing.Size(83, 18);
            this.labelCarClass.TabIndex = 4;
            this.labelCarClass.Text = "Car Class";
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelColor.Location = new System.Drawing.Point(27, 352);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(50, 18);
            this.labelColor.TabIndex = 5;
            this.labelColor.Text = "Color";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 394);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Price For Day";
            // 
            // textBoxCarModel
            // 
            this.textBoxCarModel.Location = new System.Drawing.Point(162, 266);
            this.textBoxCarModel.Name = "textBoxCarModel";
            this.textBoxCarModel.Size = new System.Drawing.Size(213, 20);
            this.textBoxCarModel.TabIndex = 8;
            // 
            // textBoxCarClass
            // 
            this.textBoxCarClass.Location = new System.Drawing.Point(162, 308);
            this.textBoxCarClass.Name = "textBoxCarClass";
            this.textBoxCarClass.Size = new System.Drawing.Size(213, 20);
            this.textBoxCarClass.TabIndex = 9;
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(162, 350);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(213, 20);
            this.textBoxColor.TabIndex = 10;
            // 
            // textBoxPriceForDay
            // 
            this.textBoxPriceForDay.Location = new System.Drawing.Point(162, 392);
            this.textBoxPriceForDay.Name = "textBoxPriceForDay";
            this.textBoxPriceForDay.Size = new System.Drawing.Size(213, 20);
            this.textBoxPriceForDay.TabIndex = 11;
            // 
            // checkBoxinRent
            // 
            this.checkBoxinRent.AutoSize = true;
            this.checkBoxinRent.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxinRent.Location = new System.Drawing.Point(162, 433);
            this.checkBoxinRent.Name = "checkBoxinRent";
            this.checkBoxinRent.Size = new System.Drawing.Size(80, 22);
            this.checkBoxinRent.TabIndex = 12;
            this.checkBoxinRent.Text = "In Rent";
            this.checkBoxinRent.UseVisualStyleBackColor = true;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearch.Location = new System.Drawing.Point(250, 175);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(302, 23);
            this.buttonSearch.TabIndex = 13;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // buttonChange
            // 
            this.buttonChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonChange.Location = new System.Drawing.Point(257, 471);
            this.buttonChange.Name = "buttonChange";
            this.buttonChange.Size = new System.Drawing.Size(118, 36);
            this.buttonChange.TabIndex = 14;
            this.buttonChange.Text = "Change";
            this.buttonChange.UseVisualStyleBackColor = true;
            this.buttonChange.Click += new System.EventHandler(this.buttonChange_Click);
            // 
            // CarDataChanging
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.Controls.Add(this.buttonChange);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.checkBoxinRent);
            this.Controls.Add(this.textBoxPriceForDay);
            this.Controls.Add(this.textBoxColor);
            this.Controls.Add(this.textBoxCarClass);
            this.Controls.Add(this.textBoxCarModel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.labelCarClass);
            this.Controls.Add(this.labelCarModel);
            this.Controls.Add(this.textBoxVinCodeForSearching);
            this.Controls.Add(this.labelVinCode);
            this.Controls.Add(this.labelCarDataChanging);
            this.Name = "CarDataChanging";
            this.Size = new System.Drawing.Size(667, 653);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCarDataChanging;
        private System.Windows.Forms.Label labelVinCode;
        private System.Windows.Forms.TextBox textBoxVinCodeForSearching;
        private System.Windows.Forms.Label labelCarModel;
        private System.Windows.Forms.Label labelCarClass;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxCarModel;
        private System.Windows.Forms.TextBox textBoxCarClass;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.TextBox textBoxPriceForDay;
        private System.Windows.Forms.CheckBox checkBoxinRent;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonChange;
    }
}
