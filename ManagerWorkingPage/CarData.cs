﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CarControllor;
using CommonLibrary;

namespace ManagerWorkingPage
{
    public partial class CarData : UserControl
    {
        public CarData()
        {
            
            InitializeComponent();
        }

        private void radioButtonFreeCars_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonFreeCars.Checked)
            {
                textBoxSearchingAttribute.ReadOnly = true;
            }
        }

        private void radioButtonInOrderCars_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonInOrderCars.Checked)
            {
                textBoxSearchingAttribute.ReadOnly = true;
            }
        }

        private void radioButtonByCarModel_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonByCarModel.Checked)
            {
                textBoxSearchingAttribute.ReadOnly = false;
            }
        }

        private void radioButtonByColor_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonByColor.Checked)
            {
                textBoxSearchingAttribute.ReadOnly = false;
            }
        }

        private void radioButtonVinCode_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonVinCode.Checked)
            {
                textBoxSearchingAttribute.ReadOnly = false;
            }
        }
        CarDataControl dataControl = new CarDataControl();
        List<Car> cars = new List<Car>();
        private void buttonSerch_Click(object sender, EventArgs e)
        {
            if (radioButtonFreeCars.Checked)
            {
                cars = dataControl.GetFreeCars();
                dataGridViewCars.DataSource = cars;
            }
            else if (radioButtonInOrderCars.Checked)
            {
                cars = dataControl.GetCarsInOrder();
                dataGridViewCars.DataSource = cars;
            }
            else if (radioButtonByCarModel.Checked)
            {
                cars = dataControl.GetCarsByModel(textBoxSearchingAttribute.Text);
                dataGridViewCars.DataSource = cars;
            }
            else if (radioButtonByColor.Checked)
            {
                cars = dataControl.GetCarsByColor(textBoxSearchingAttribute.Text);
                dataGridViewCars.DataSource = cars;
            }
            else if (radioButtonVinCode.Checked)
            {
                List<Car> cars = dataControl.GetCarByVinCode(textBoxSearchingAttribute.Text);
                dataGridViewCars.DataSource = cars;
            }
        }
    }
}
