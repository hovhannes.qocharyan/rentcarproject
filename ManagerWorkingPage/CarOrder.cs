﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OrderControllor;
using CommonLibrary.Manager;
using CommonControl;
using CommonLibrary.Customer;
using CommonLibrary;
using CommonLibrary.Order;

namespace ManagerWorkingPage
{
    public partial class CarOrder : UserControl
    {
        public static Manager mg;
        public CarOrder()
        {
            InitializeComponent();
        }

        private void CarOrder_Load(object sender, EventArgs e)
        {

        }

        private void buttonChakeOrderDate_Click(object sender, EventArgs e)
        {
            try
            {


                if (CheckingInputDataControl.PassportCodeChecking(textBoxCustomerPassportCode.Text))
                {
                    labelCustomerPassportCodeCheckSimvole.BackColor = Color.Green;
                    labelCustomerPassportCodeCheckSimvole.Text = "Yes";
                }
                else
                {
                    labelCustomerPassportCodeCheckSimvole.BackColor = Color.Red;
                    labelCustomerPassportCodeCheckSimvole.Text = "No";
                    throw new ArgumentException("Cant find customer with this passport code");
                }

                if (CheckingInputDataControl.VinCodeChacking(textBoxCarVinCode.Text))
                {
                    labelCarVinCodeCheckingSimvole.BackColor = Color.Green;
                    labelCarVinCodeCheckingSimvole.Text = "Yes";
                }
                else
                {
                    labelCarVinCodeCheckingSimvole.BackColor = Color.Red;
                    labelCarVinCodeCheckingSimvole.Text = "No";
                    throw new ArgumentException("Cant find car with this vincode or the car is in order");
                }

                int.TryParse(textBoxStartYear.Text, out int startYear);
                int.TryParse(textBoxStartMonth.Text, out int startMonth);
                int.TryParse(textBoxStartDay.Text, out int startDay);

                DateTime startDate = new DateTime(startYear, startMonth, startDay);

                int.TryParse(textBoxEndYear.Text, out int endYear);
                int.TryParse(textBoxEndMonth.Text, out int endMonth);
                int.TryParse(textBoxEndDay.Text, out int endDay);

                DateTime endDate = new DateTime(endYear, endMonth, endDay);

                if (CheckingInputDataControl.RentDateChaking(startDate, endDate))
                {
                    labelStartDateValidationSimvole.BackColor = Color.Green;
                    labelStartDateValidationSimvole.Text = "Yes";
                    labelEndDateChaikingSimvole.BackColor = Color.Green;
                    labelEndDateChaikingSimvole.Text = "Yes";
                }
                else
                {
                    labelStartDateValidationSimvole.BackColor = Color.Red;
                    labelStartDateValidationSimvole.Text = "No";
                    labelEndDateChaikingSimvole.BackColor = Color.Red;
                    labelEndDateChaikingSimvole.Text = "No";
                    throw new ArgumentException("The entred date is incorect");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }


        }

        private void buttonOrder_Click(object sender, EventArgs e)
        {
            try
            {


                buttonChakeOrderDate_Click(sender, e);
                Customer customer = Helper.GetCustomer(textBoxCustomerPassportCode.Text);
                Car car = Helper.GetCar(textBoxCarVinCode.Text);
                decimal.TryParse(textBoxTotalPrice.Text, out decimal price);
                Manager manager = Helper.GetManager(mg.UserName);

                int.TryParse(textBoxStartYear.Text, out int startYear);
                int.TryParse(textBoxStartMonth.Text, out int startMonth);
                int.TryParse(textBoxStartDay.Text, out int startDay);

                DateTime startDate = new DateTime(startYear, startMonth, startDay);

                int.TryParse(textBoxEndYear.Text, out int endYear);
                int.TryParse(textBoxEndMonth.Text, out int endMonth);
                int.TryParse(textBoxEndDay.Text, out int endDay);

                DateTime endDate = new DateTime(endYear, endMonth, endDay);

                OrderControl orderControl = new OrderControl();
                orderControl.DoOrder(customer, manager, car, price, startDate, endDate);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

            





        }
    }
}
