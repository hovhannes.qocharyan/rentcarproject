﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CarControllor;
using CommonLibrary;

namespace ManagerWorkingPage
{
    public partial class CarDataChanging : UserControl
    {
        public CarDataChanging()
        {
            InitializeComponent();
        }

        private string vinCode;

        private void buttonSearch_Click(object sender, EventArgs e)
        {
            try
            {


                CarDataControl dataControl = new CarDataControl();
                vinCode = textBoxVinCodeForSearching.Text;
                var car = dataControl.GetCarByVinCode(textBoxVinCodeForSearching.Text).FirstOrDefault();
                if (car == null)
                {
                    MessageBox.Show("Not found , vincode is incorect");
                }

                textBoxCarModel.Text = car.CarModel;
                textBoxCarClass.Text = car.CarClass;
                textBoxColor.Text = car.Color;
                textBoxPriceForDay.Text = car.Price.ToString();
                if (car.InRent == true)
                {
                    checkBoxinRent.Checked = true;
                }
                else
                {
                    checkBoxinRent.Checked = false;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            
        }

        private void buttonChange_Click(object sender, EventArgs e)
        {
            try
            {


                string carModel = textBoxCarModel.Text;
                string carClass = textBoxCarClass.Text;
                string color = textBoxColor.Text;
                decimal priceForDay = decimal.Parse(textBoxPriceForDay.Text);
                bool inRent;
                if (checkBoxinRent.Checked == true)
                {
                    inRent = true;

                }
                else
                {
                    inRent = false;
                }
                Car car = new Car(vinCode, carModel, carClass, color, priceForDay);
                car.InRent = inRent;
                CarDataChangingControl carDataChangingControl = new CarDataChangingControl();
                carDataChangingControl.UpdateCarData(car);

                MessageBox.Show("Car data sucsefuly changed");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }
    }
}
