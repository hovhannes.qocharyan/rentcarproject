﻿namespace ManagerWorkingPage
{
    partial class CustomerRegistration
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCustomerRegistration = new System.Windows.Forms.Label();
            this.labelFname = new System.Windows.Forms.Label();
            this.labelMName = new System.Windows.Forms.Label();
            this.labelLName = new System.Windows.Forms.Label();
            this.labelPhoneNumber = new System.Windows.Forms.Label();
            this.labelBDay = new System.Windows.Forms.Label();
            this.labelPassportCode = new System.Windows.Forms.Label();
            this.labelDrivingLicence = new System.Windows.Forms.Label();
            this.textBoxFName = new System.Windows.Forms.TextBox();
            this.textBoxMName = new System.Windows.Forms.TextBox();
            this.textBoxLName = new System.Windows.Forms.TextBox();
            this.textBoxPhoneNumber = new System.Windows.Forms.TextBox();
            this.textBoxDay = new System.Windows.Forms.TextBox();
            this.textBoxPassportCode = new System.Windows.Forms.TextBox();
            this.textBoxDrivinglicence = new System.Windows.Forms.TextBox();
            this.textBoxMonth = new System.Windows.Forms.TextBox();
            this.textBoxYear = new System.Windows.Forms.TextBox();
            this.labelDay = new System.Windows.Forms.Label();
            this.labelmonth = new System.Windows.Forms.Label();
            this.labelYear = new System.Windows.Forms.Label();
            this.buttonRegistr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelCustomerRegistration
            // 
            this.labelCustomerRegistration.AutoSize = true;
            this.labelCustomerRegistration.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCustomerRegistration.Location = new System.Drawing.Point(166, 29);
            this.labelCustomerRegistration.Name = "labelCustomerRegistration";
            this.labelCustomerRegistration.Size = new System.Drawing.Size(306, 31);
            this.labelCustomerRegistration.TabIndex = 0;
            this.labelCustomerRegistration.Text = "Customer Registration";
            // 
            // labelFname
            // 
            this.labelFname.AutoSize = true;
            this.labelFname.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFname.Location = new System.Drawing.Point(20, 138);
            this.labelFname.Name = "labelFname";
            this.labelFname.Size = new System.Drawing.Size(91, 18);
            this.labelFname.TabIndex = 1;
            this.labelFname.Text = "First Name";
            // 
            // labelMName
            // 
            this.labelMName.AutoSize = true;
            this.labelMName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMName.Location = new System.Drawing.Point(20, 182);
            this.labelMName.Name = "labelMName";
            this.labelMName.Size = new System.Drawing.Size(106, 18);
            this.labelMName.TabIndex = 2;
            this.labelMName.Text = "Middle Name";
            // 
            // labelLName
            // 
            this.labelLName.AutoSize = true;
            this.labelLName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLName.Location = new System.Drawing.Point(20, 226);
            this.labelLName.Name = "labelLName";
            this.labelLName.Size = new System.Drawing.Size(89, 18);
            this.labelLName.TabIndex = 3;
            this.labelLName.Text = "Last Name";
            // 
            // labelPhoneNumber
            // 
            this.labelPhoneNumber.AutoSize = true;
            this.labelPhoneNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPhoneNumber.Location = new System.Drawing.Point(20, 270);
            this.labelPhoneNumber.Name = "labelPhoneNumber";
            this.labelPhoneNumber.Size = new System.Drawing.Size(120, 18);
            this.labelPhoneNumber.TabIndex = 4;
            this.labelPhoneNumber.Text = "Phone Number";
            // 
            // labelBDay
            // 
            this.labelBDay.AutoSize = true;
            this.labelBDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBDay.Location = new System.Drawing.Point(20, 314);
            this.labelBDay.Name = "labelBDay";
            this.labelBDay.Size = new System.Drawing.Size(77, 18);
            this.labelBDay.TabIndex = 5;
            this.labelBDay.Text = "Birth Day";
            // 
            // labelPassportCode
            // 
            this.labelPassportCode.AutoSize = true;
            this.labelPassportCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPassportCode.Location = new System.Drawing.Point(20, 358);
            this.labelPassportCode.Name = "labelPassportCode";
            this.labelPassportCode.Size = new System.Drawing.Size(121, 18);
            this.labelPassportCode.TabIndex = 6;
            this.labelPassportCode.Text = "Passport Code";
            // 
            // labelDrivingLicence
            // 
            this.labelDrivingLicence.AutoSize = true;
            this.labelDrivingLicence.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDrivingLicence.Location = new System.Drawing.Point(20, 402);
            this.labelDrivingLicence.Name = "labelDrivingLicence";
            this.labelDrivingLicence.Size = new System.Drawing.Size(123, 18);
            this.labelDrivingLicence.TabIndex = 7;
            this.labelDrivingLicence.Text = "Driving Licence";
            // 
            // textBoxFName
            // 
            this.textBoxFName.Location = new System.Drawing.Point(166, 131);
            this.textBoxFName.Name = "textBoxFName";
            this.textBoxFName.Size = new System.Drawing.Size(369, 20);
            this.textBoxFName.TabIndex = 8;
            // 
            // textBoxMName
            // 
            this.textBoxMName.Location = new System.Drawing.Point(166, 175);
            this.textBoxMName.Name = "textBoxMName";
            this.textBoxMName.Size = new System.Drawing.Size(369, 20);
            this.textBoxMName.TabIndex = 9;
            // 
            // textBoxLName
            // 
            this.textBoxLName.Location = new System.Drawing.Point(166, 219);
            this.textBoxLName.Name = "textBoxLName";
            this.textBoxLName.Size = new System.Drawing.Size(369, 20);
            this.textBoxLName.TabIndex = 10;
            // 
            // textBoxPhoneNumber
            // 
            this.textBoxPhoneNumber.Location = new System.Drawing.Point(166, 263);
            this.textBoxPhoneNumber.Name = "textBoxPhoneNumber";
            this.textBoxPhoneNumber.Size = new System.Drawing.Size(369, 20);
            this.textBoxPhoneNumber.TabIndex = 11;
            // 
            // textBoxDay
            // 
            this.textBoxDay.Location = new System.Drawing.Point(207, 314);
            this.textBoxDay.Name = "textBoxDay";
            this.textBoxDay.Size = new System.Drawing.Size(60, 20);
            this.textBoxDay.TabIndex = 12;
            // 
            // textBoxPassportCode
            // 
            this.textBoxPassportCode.Location = new System.Drawing.Point(166, 351);
            this.textBoxPassportCode.Name = "textBoxPassportCode";
            this.textBoxPassportCode.Size = new System.Drawing.Size(369, 20);
            this.textBoxPassportCode.TabIndex = 13;
            // 
            // textBoxDrivinglicence
            // 
            this.textBoxDrivinglicence.Location = new System.Drawing.Point(166, 395);
            this.textBoxDrivinglicence.Name = "textBoxDrivinglicence";
            this.textBoxDrivinglicence.Size = new System.Drawing.Size(369, 20);
            this.textBoxDrivinglicence.TabIndex = 14;
            // 
            // textBoxMonth
            // 
            this.textBoxMonth.Location = new System.Drawing.Point(341, 312);
            this.textBoxMonth.Name = "textBoxMonth";
            this.textBoxMonth.Size = new System.Drawing.Size(60, 20);
            this.textBoxMonth.TabIndex = 15;
            // 
            // textBoxYear
            // 
            this.textBoxYear.Location = new System.Drawing.Point(475, 312);
            this.textBoxYear.Name = "textBoxYear";
            this.textBoxYear.Size = new System.Drawing.Size(60, 20);
            this.textBoxYear.TabIndex = 16;
            // 
            // labelDay
            // 
            this.labelDay.AutoSize = true;
            this.labelDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDay.Location = new System.Drawing.Point(163, 318);
            this.labelDay.Name = "labelDay";
            this.labelDay.Size = new System.Drawing.Size(36, 16);
            this.labelDay.TabIndex = 17;
            this.labelDay.Text = "Day";
            // 
            // labelmonth
            // 
            this.labelmonth.AutoSize = true;
            this.labelmonth.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelmonth.Location = new System.Drawing.Point(286, 318);
            this.labelmonth.Name = "labelmonth";
            this.labelmonth.Size = new System.Drawing.Size(49, 16);
            this.labelmonth.TabIndex = 18;
            this.labelmonth.Text = "Month";
            // 
            // labelYear
            // 
            this.labelYear.AutoSize = true;
            this.labelYear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelYear.Location = new System.Drawing.Point(428, 318);
            this.labelYear.Name = "labelYear";
            this.labelYear.Size = new System.Drawing.Size(41, 16);
            this.labelYear.TabIndex = 19;
            this.labelYear.Text = "Year";
            // 
            // buttonRegistr
            // 
            this.buttonRegistr.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegistr.Location = new System.Drawing.Point(252, 487);
            this.buttonRegistr.Name = "buttonRegistr";
            this.buttonRegistr.Size = new System.Drawing.Size(149, 53);
            this.buttonRegistr.TabIndex = 20;
            this.buttonRegistr.Text = "Registr";
            this.buttonRegistr.UseVisualStyleBackColor = true;
            this.buttonRegistr.Click += new System.EventHandler(this.buttonRegistr_Click);
            // 
            // CustomerRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.buttonRegistr);
            this.Controls.Add(this.labelYear);
            this.Controls.Add(this.labelmonth);
            this.Controls.Add(this.labelDay);
            this.Controls.Add(this.textBoxYear);
            this.Controls.Add(this.textBoxMonth);
            this.Controls.Add(this.textBoxDrivinglicence);
            this.Controls.Add(this.textBoxPassportCode);
            this.Controls.Add(this.textBoxDay);
            this.Controls.Add(this.textBoxPhoneNumber);
            this.Controls.Add(this.textBoxLName);
            this.Controls.Add(this.textBoxMName);
            this.Controls.Add(this.textBoxFName);
            this.Controls.Add(this.labelDrivingLicence);
            this.Controls.Add(this.labelPassportCode);
            this.Controls.Add(this.labelBDay);
            this.Controls.Add(this.labelPhoneNumber);
            this.Controls.Add(this.labelLName);
            this.Controls.Add(this.labelMName);
            this.Controls.Add(this.labelFname);
            this.Controls.Add(this.labelCustomerRegistration);
            this.Name = "CustomerRegistration";
            this.Size = new System.Drawing.Size(667, 653);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCustomerRegistration;
        private System.Windows.Forms.Label labelFname;
        private System.Windows.Forms.Label labelMName;
        private System.Windows.Forms.Label labelLName;
        private System.Windows.Forms.Label labelPhoneNumber;
        private System.Windows.Forms.Label labelBDay;
        private System.Windows.Forms.Label labelPassportCode;
        private System.Windows.Forms.Label labelDrivingLicence;
        private System.Windows.Forms.TextBox textBoxFName;
        private System.Windows.Forms.TextBox textBoxMName;
        private System.Windows.Forms.TextBox textBoxLName;
        private System.Windows.Forms.TextBox textBoxPhoneNumber;
        private System.Windows.Forms.TextBox textBoxDay;
        private System.Windows.Forms.TextBox textBoxPassportCode;
        private System.Windows.Forms.TextBox textBoxDrivinglicence;
        private System.Windows.Forms.TextBox textBoxMonth;
        private System.Windows.Forms.TextBox textBoxYear;
        private System.Windows.Forms.Label labelDay;
        private System.Windows.Forms.Label labelmonth;
        private System.Windows.Forms.Label labelYear;
        private System.Windows.Forms.Button buttonRegistr;
    }
}
