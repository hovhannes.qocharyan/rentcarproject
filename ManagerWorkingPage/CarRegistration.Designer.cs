﻿namespace ManagerWorkingPage
{
    partial class CarRegistration
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelCarRegistrationPage = new System.Windows.Forms.Label();
            this.labelVinCode = new System.Windows.Forms.Label();
            this.labelCarModel = new System.Windows.Forms.Label();
            this.labelCarClass = new System.Windows.Forms.Label();
            this.labelColor = new System.Windows.Forms.Label();
            this.labelPriceForDay = new System.Windows.Forms.Label();
            this.textBoxVinCode = new System.Windows.Forms.TextBox();
            this.textBoxCarModel = new System.Windows.Forms.TextBox();
            this.textBoxCarClass = new System.Windows.Forms.TextBox();
            this.textBoxColor = new System.Windows.Forms.TextBox();
            this.textBoxPriceForDay = new System.Windows.Forms.TextBox();
            this.buttonCarRegistration = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelCarRegistrationPage
            // 
            this.labelCarRegistrationPage.AutoSize = true;
            this.labelCarRegistrationPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarRegistrationPage.Location = new System.Drawing.Point(219, 31);
            this.labelCarRegistrationPage.Name = "labelCarRegistrationPage";
            this.labelCarRegistrationPage.Size = new System.Drawing.Size(227, 31);
            this.labelCarRegistrationPage.TabIndex = 0;
            this.labelCarRegistrationPage.Text = "Car Registration";
            // 
            // labelVinCode
            // 
            this.labelVinCode.AutoSize = true;
            this.labelVinCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVinCode.Location = new System.Drawing.Point(17, 122);
            this.labelVinCode.Name = "labelVinCode";
            this.labelVinCode.Size = new System.Drawing.Size(77, 20);
            this.labelVinCode.TabIndex = 1;
            this.labelVinCode.Text = "VinCode";
            // 
            // labelCarModel
            // 
            this.labelCarModel.AutoSize = true;
            this.labelCarModel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarModel.Location = new System.Drawing.Point(17, 173);
            this.labelCarModel.Name = "labelCarModel";
            this.labelCarModel.Size = new System.Drawing.Size(90, 20);
            this.labelCarModel.TabIndex = 2;
            this.labelCarModel.Text = "Car Model";
            // 
            // labelCarClass
            // 
            this.labelCarClass.AutoSize = true;
            this.labelCarClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarClass.Location = new System.Drawing.Point(17, 224);
            this.labelCarClass.Name = "labelCarClass";
            this.labelCarClass.Size = new System.Drawing.Size(86, 20);
            this.labelCarClass.TabIndex = 3;
            this.labelCarClass.Text = "Car Class";
            // 
            // labelColor
            // 
            this.labelColor.AutoSize = true;
            this.labelColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelColor.Location = new System.Drawing.Point(17, 275);
            this.labelColor.Name = "labelColor";
            this.labelColor.Size = new System.Drawing.Size(51, 20);
            this.labelColor.TabIndex = 4;
            this.labelColor.Text = "Color";
            // 
            // labelPriceForDay
            // 
            this.labelPriceForDay.AutoSize = true;
            this.labelPriceForDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPriceForDay.Location = new System.Drawing.Point(17, 326);
            this.labelPriceForDay.Name = "labelPriceForDay";
            this.labelPriceForDay.Size = new System.Drawing.Size(117, 20);
            this.labelPriceForDay.TabIndex = 5;
            this.labelPriceForDay.Text = "Price For Day";
            // 
            // textBoxVinCode
            // 
            this.textBoxVinCode.Location = new System.Drawing.Point(225, 121);
            this.textBoxVinCode.Name = "textBoxVinCode";
            this.textBoxVinCode.Size = new System.Drawing.Size(364, 20);
            this.textBoxVinCode.TabIndex = 6;
            // 
            // textBoxCarModel
            // 
            this.textBoxCarModel.Location = new System.Drawing.Point(225, 172);
            this.textBoxCarModel.Name = "textBoxCarModel";
            this.textBoxCarModel.Size = new System.Drawing.Size(364, 20);
            this.textBoxCarModel.TabIndex = 7;
            // 
            // textBoxCarClass
            // 
            this.textBoxCarClass.Location = new System.Drawing.Point(225, 223);
            this.textBoxCarClass.Name = "textBoxCarClass";
            this.textBoxCarClass.Size = new System.Drawing.Size(364, 20);
            this.textBoxCarClass.TabIndex = 8;
            // 
            // textBoxColor
            // 
            this.textBoxColor.Location = new System.Drawing.Point(225, 274);
            this.textBoxColor.Name = "textBoxColor";
            this.textBoxColor.Size = new System.Drawing.Size(364, 20);
            this.textBoxColor.TabIndex = 9;
            // 
            // textBoxPriceForDay
            // 
            this.textBoxPriceForDay.Location = new System.Drawing.Point(225, 325);
            this.textBoxPriceForDay.Name = "textBoxPriceForDay";
            this.textBoxPriceForDay.Size = new System.Drawing.Size(364, 20);
            this.textBoxPriceForDay.TabIndex = 10;
            // 
            // buttonCarRegistration
            // 
            this.buttonCarRegistration.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarRegistration.Location = new System.Drawing.Point(286, 423);
            this.buttonCarRegistration.Name = "buttonCarRegistration";
            this.buttonCarRegistration.Size = new System.Drawing.Size(146, 75);
            this.buttonCarRegistration.TabIndex = 11;
            this.buttonCarRegistration.Text = "Registr Car";
            this.buttonCarRegistration.UseVisualStyleBackColor = true;
            this.buttonCarRegistration.Click += new System.EventHandler(this.buttonCarRegistration_Click);
            // 
            // CarRegistration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.buttonCarRegistration);
            this.Controls.Add(this.textBoxPriceForDay);
            this.Controls.Add(this.textBoxColor);
            this.Controls.Add(this.textBoxCarClass);
            this.Controls.Add(this.textBoxCarModel);
            this.Controls.Add(this.textBoxVinCode);
            this.Controls.Add(this.labelPriceForDay);
            this.Controls.Add(this.labelColor);
            this.Controls.Add(this.labelCarClass);
            this.Controls.Add(this.labelCarModel);
            this.Controls.Add(this.labelVinCode);
            this.Controls.Add(this.labelCarRegistrationPage);
            this.Name = "CarRegistration";
            this.Size = new System.Drawing.Size(667, 653);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCarRegistrationPage;
        private System.Windows.Forms.Label labelVinCode;
        private System.Windows.Forms.Label labelCarModel;
        private System.Windows.Forms.Label labelCarClass;
        private System.Windows.Forms.Label labelColor;
        private System.Windows.Forms.Label labelPriceForDay;
        private System.Windows.Forms.TextBox textBoxVinCode;
        private System.Windows.Forms.TextBox textBoxCarModel;
        private System.Windows.Forms.TextBox textBoxCarClass;
        private System.Windows.Forms.TextBox textBoxColor;
        private System.Windows.Forms.TextBox textBoxPriceForDay;
        private System.Windows.Forms.Button buttonCarRegistration;
    }
}
