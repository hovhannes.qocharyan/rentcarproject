﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CarControllor;

namespace ManagerWorkingPage
{
    public partial class CarRegistration : UserControl
    {
        public CarRegistration()
        {
            InitializeComponent();
        }

        private void buttonCarRegistration_Click(object sender, EventArgs e)
        {
            try
            {

                string vinCode = textBoxVinCode.Text;
                string carModel = textBoxCarModel.Text;
                string carClass = textBoxCarClass.Text;
                string color = textBoxColor.Text;
                decimal priceForDay = decimal.Parse(textBoxPriceForDay.Text);

                CarRegistrationControl.CarRegistration(vinCode, carModel, carClass, color, priceForDay);

                MessageBox.Show("Car registration is complite");
                vinCode = null;
                carModel = null;
                carClass = null;
                color = null;
                priceForDay = 0;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

        }
    }
}
