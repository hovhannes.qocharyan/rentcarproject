﻿namespace ManagerWorkingPage
{
    partial class ManagerWorkingPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonCustomerRegistration = new System.Windows.Forms.Button();
            this.buttonCarDataChanging = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.carData1 = new CarData();
            this.carRegistration1 = new CarRegistration();
            this.buttonCarData = new System.Windows.Forms.Button();
            this.buttonCarRegistration = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.carDataChanging1 = new CarDataChanging();
            this.carData2 = new CarData();
            this.carRegistration2 = new CarRegistration();
            this.customerRegistration1 = new CustomerRegistration();
            this.carOrder1 = new CarOrder();
            this.buttonCarOrder = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonCarOrder);
            this.panel1.Controls.Add(this.buttonCustomerRegistration);
            this.panel1.Controls.Add(this.buttonCarDataChanging);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.buttonCarData);
            this.panel1.Controls.Add(this.buttonCarRegistration);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(128, 653);
            this.panel1.TabIndex = 0;
            // 
            // buttonCustomerRegistration
            // 
            this.buttonCustomerRegistration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCustomerRegistration.Location = new System.Drawing.Point(0, 243);
            this.buttonCustomerRegistration.Name = "buttonCustomerRegistration";
            this.buttonCustomerRegistration.Size = new System.Drawing.Size(121, 74);
            this.buttonCustomerRegistration.TabIndex = 5;
            this.buttonCustomerRegistration.Text = "Customer Registration";
            this.buttonCustomerRegistration.UseVisualStyleBackColor = true;
            this.buttonCustomerRegistration.Click += new System.EventHandler(this.buttonCustomerRegistration_Click);
            // 
            // buttonCarDataChanging
            // 
            this.buttonCarDataChanging.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarDataChanging.Location = new System.Drawing.Point(0, 163);
            this.buttonCarDataChanging.Name = "buttonCarDataChanging";
            this.buttonCarDataChanging.Size = new System.Drawing.Size(121, 74);
            this.buttonCarDataChanging.TabIndex = 4;
            this.buttonCarDataChanging.Text = "Car Data Changing";
            this.buttonCarDataChanging.UseVisualStyleBackColor = true;
            this.buttonCarDataChanging.Click += new System.EventHandler(this.buttonCarDataChanging_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.carData1);
            this.panel3.Controls.Add(this.carRegistration1);
            this.panel3.Location = new System.Drawing.Point(128, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(630, 653);
            this.panel3.TabIndex = 1;
            // 
            // carData1
            // 
            this.carData1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.carData1.Location = new System.Drawing.Point(65, 27);
            this.carData1.Name = "carData1";
            this.carData1.Size = new System.Drawing.Size(667, 653);
            this.carData1.TabIndex = 1;
            // 
            // carRegistration1
            // 
            this.carRegistration1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.carRegistration1.Location = new System.Drawing.Point(34, 83);
            this.carRegistration1.Name = "carRegistration1";
            this.carRegistration1.Size = new System.Drawing.Size(667, 653);
            this.carRegistration1.TabIndex = 0;
            this.carRegistration1.Load += new System.EventHandler(this.carRegistration1_Load);
            // 
            // buttonCarData
            // 
            this.buttonCarData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarData.Location = new System.Drawing.Point(1, 83);
            this.buttonCarData.Name = "buttonCarData";
            this.buttonCarData.Size = new System.Drawing.Size(121, 74);
            this.buttonCarData.TabIndex = 3;
            this.buttonCarData.Text = "Car Data";
            this.buttonCarData.UseVisualStyleBackColor = true;
            this.buttonCarData.Click += new System.EventHandler(this.buttonCarData_Click);
            // 
            // buttonCarRegistration
            // 
            this.buttonCarRegistration.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarRegistration.Location = new System.Drawing.Point(1, 3);
            this.buttonCarRegistration.Name = "buttonCarRegistration";
            this.buttonCarRegistration.Size = new System.Drawing.Size(121, 74);
            this.buttonCarRegistration.TabIndex = 2;
            this.buttonCarRegistration.Text = "Car Registration";
            this.buttonCarRegistration.UseVisualStyleBackColor = true;
            this.buttonCarRegistration.Click += new System.EventHandler(this.buttonCarRegistration_Click);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(128, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(727, 708);
            this.panel2.TabIndex = 1;
            // 
            // carDataChanging1
            // 
            this.carDataChanging1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.carDataChanging1.Location = new System.Drawing.Point(130, 3);
            this.carDataChanging1.Name = "carDataChanging1";
            this.carDataChanging1.Size = new System.Drawing.Size(667, 653);
            this.carDataChanging1.TabIndex = 3;
            // 
            // carData2
            // 
            this.carData2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.carData2.Location = new System.Drawing.Point(130, 3);
            this.carData2.Name = "carData2";
            this.carData2.Size = new System.Drawing.Size(667, 653);
            this.carData2.TabIndex = 2;
            // 
            // carRegistration2
            // 
            this.carRegistration2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.carRegistration2.Location = new System.Drawing.Point(130, 3);
            this.carRegistration2.Name = "carRegistration2";
            this.carRegistration2.Size = new System.Drawing.Size(667, 653);
            this.carRegistration2.TabIndex = 1;
            // 
            // customerRegistration1
            // 
            this.customerRegistration1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.customerRegistration1.Location = new System.Drawing.Point(130, 3);
            this.customerRegistration1.Name = "customerRegistration1";
            this.customerRegistration1.Size = new System.Drawing.Size(667, 653);
            this.customerRegistration1.TabIndex = 4;
            // 
            // carOrder1
            // 
            this.carOrder1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.carOrder1.Location = new System.Drawing.Point(130, 3);
            this.carOrder1.Name = "carOrder1";
            this.carOrder1.Size = new System.Drawing.Size(667, 653);
            this.carOrder1.TabIndex = 5;
            // 
            // buttonCarOrder
            // 
            this.buttonCarOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCarOrder.Location = new System.Drawing.Point(1, 323);
            this.buttonCarOrder.Name = "buttonCarOrder";
            this.buttonCarOrder.Size = new System.Drawing.Size(121, 74);
            this.buttonCarOrder.TabIndex = 6;
            this.buttonCarOrder.Text = "Car Order";
            this.buttonCarOrder.UseVisualStyleBackColor = true;
            this.buttonCarOrder.Click += new System.EventHandler(this.buttonCarOrder_Click);
            // 
            // ManagerWorkingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(798, 659);
            this.Controls.Add(this.carOrder1);
            this.Controls.Add(this.customerRegistration1);
            this.Controls.Add(this.carDataChanging1);
            this.Controls.Add(this.carData2);
            this.Controls.Add(this.carRegistration2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "ManagerWorkingPage";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.ManagerWorkingPage_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonCarRegistration;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private CarRegistration carRegistration1;
        private CarData carData1;
        private System.Windows.Forms.Button buttonCarData;
        private CarRegistration carRegistration2;
        private CarData carData2;
        private System.Windows.Forms.Button buttonCarDataChanging;
        private CarDataChanging carDataChanging1;
        private System.Windows.Forms.Button buttonCustomerRegistration;
        private CustomerRegistration customerRegistration1;
        private System.Windows.Forms.Button buttonCarOrder;
        private CarOrder carOrder1;
    }
}

