﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AcountControllor;

namespace ManagerWorkingPage
{
    public partial class CustomerRegistration : UserControl
    {
        public CustomerRegistration()
        {
            InitializeComponent();
        }

        private void buttonRegistr_Click(object sender, EventArgs e)
        {
            try
            {


                string fName = textBoxFName.Text;
                string mName = textBoxMName.Text;
                string lName = textBoxLName.Text;
                string phoneNumber = textBoxPhoneNumber.Text;
                DateTime bDay = new DateTime(int.Parse(textBoxYear.Text), int.Parse(textBoxMonth.Text), int.Parse(textBoxDay.Text));
                string passportCode = textBoxPassportCode.Text;
                string drivingLicence = textBoxDrivinglicence.Text;

                RegistrationControl.CustomerRegistration(fName, mName, lName, phoneNumber, bDay, passportCode, drivingLicence);
                MessageBox.Show("Customer registration is complite");
                textBoxFName.Text = null;
                textBoxMName.Text = null;
                textBoxLName.Text = null;
                textBoxPhoneNumber.Text = null;
                textBoxYear.Text = null;
                textBoxMonth.Text = null;
                textBoxDay.Text = null;
                textBoxPassportCode.Text= null;
                textBoxDrivinglicence.Text= null;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }

        }
    }
}
